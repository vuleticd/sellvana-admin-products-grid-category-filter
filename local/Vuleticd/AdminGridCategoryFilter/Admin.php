<?php
/*
# NOTICE OF LICENSE
#
# This source file is subject to the Open Software License (OSL 3.0)
# that is available through the world-wide-web at this URL:
# http://opensource.org/licenses/osl-3.0.php
#
# @category    Vuleticd
# @package     Vuleticd_AdminGridCategoryFilter
# @copyright   Copyright (c) 2014 Vuletic Dragan (http://www.vuleticd.com)
# @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/
class Vuleticd_AdminGridCategoryFilter_Admin
{

    public static function onGridView($args)
    {
        $view = $args['view'];
        $grid = $view->getGrid();  
        $orm = $grid['config']['orm'];
        
        $orm->select_expr('GROUP_CONCAT(DISTINCT c.full_name)', 'categories')
                    ->left_outer_join('FCom_Catalog_Model_CategoryProduct', array('p.id','=','cp.product_id'), 'cp')
                    ->left_outer_join('FCom_Catalog_Model_Category', array('cp.category_id','=','c.id'), 'c')
                    ->group_by('p.id')
        ;

        // Add columns
        $grid['config']['columns'][] = array('name'=>'categories', 'label'=>'In Category', 'index' => 'c.full_name');
        // Add Filters
        $grid['config']['filters'][] = array('field'=>'categories', 'type'=>'text');
        
        $view->set('grid', $grid);
    }

}